import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  employeeId: number;

  @Column({
    length: '32',
  })
  name: string;

  @Column({
    unique: true,
    length: '64',
  })
  email: string;

  @Column({
    length: '128',
  })
  password: string;

  @Column()
  role: string;
}
