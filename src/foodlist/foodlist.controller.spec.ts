import { Test, TestingModule } from '@nestjs/testing';
import { FoodlistController } from './foodlist.controller';
import { FoodlistService } from './foodlist.service';

describe('FoodlistController', () => {
  let controller: FoodlistController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [FoodlistController],
      providers: [FoodlistService],
    }).compile();

    controller = module.get<FoodlistController>(FoodlistController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
