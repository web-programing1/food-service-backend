import { IsNotEmpty, IsNumber } from 'class-validator';
import { CreateFoodDto } from 'src/food/dto/create-food.dto';

export class CreateFoodlistDto {
  @IsNotEmpty()
  food: CreateFoodDto;

  @IsNotEmpty()
  orderStatus: string;
}
