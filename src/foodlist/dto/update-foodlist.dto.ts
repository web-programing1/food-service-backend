import { PartialType } from '@nestjs/mapped-types';
import { CreateFoodlistDto } from './create-foodlist.dto';

export class UpdateFoodlistDto extends PartialType(CreateFoodlistDto) {}
