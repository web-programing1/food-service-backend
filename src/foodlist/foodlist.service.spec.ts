import { Test, TestingModule } from '@nestjs/testing';
import { FoodlistService } from './foodlist.service';

describe('FoodlistService', () => {
  let service: FoodlistService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [FoodlistService],
    }).compile();

    service = module.get<FoodlistService>(FoodlistService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
