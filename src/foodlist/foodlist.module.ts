import { Module } from '@nestjs/common';
import { FoodlistService } from './foodlist.service';
import { FoodlistController } from './foodlist.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Foodlist } from './entities/foodlist.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Foodlist])],
  controllers: [FoodlistController],
  providers: [FoodlistService],
  exports: [FoodlistService],
})
export class FoodlistModule {}
