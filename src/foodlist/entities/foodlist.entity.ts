import { Food } from 'src/food/entities/food.entity';
import { OrderItem } from 'src/order/entities/order-item';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  ManyToMany,
  JoinTable,
  ManyToOne,
} from 'typeorm';

@Entity()
export class Foodlist {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => Food)
  food: Food;

  @CreateDateColumn()
  orderTime: Date;

  @Column() // รอทำอาหาร, กำลังทำอาหาร, พร้อมเสริฟอาหาร, เสร็จสิ้น, ยกเลิก
  orderStatus: string;

  @ManyToOne(() => OrderItem, (orderItem) => orderItem.foodlists)
  orderItem: OrderItem;

  @DeleteDateColumn()
  servedTime: Date;
}
