import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectDataSource, InjectRepository } from '@nestjs/typeorm';
import { DataSource, Repository } from 'typeorm';
import { CreateFoodlistDto } from './dto/create-foodlist.dto';
import { UpdateFoodlistDto } from './dto/update-foodlist.dto';
import { Foodlist } from './entities/foodlist.entity';

@Injectable()
export class FoodlistService {
  constructor(
    @InjectDataSource()
    private dataSource: DataSource,
    @InjectRepository(Foodlist)
    private foodlistRepository: Repository<Foodlist>,
  ) {}

  findByServeReady() {
    return this.dataSource.query(
      'SELECT foodlist.id, food.name, "order".tableId FROM "foodlist" INNER JOIN order_item ON order_item.id = foodlist.orderItemId INNER JOIN "order" ON "order".id = order_item.orderId INNER JOIN food ON food.id = foodlist.foodId WHERE foodlist.orderStatus == "พร้อมเสริฟอาหาร" GROUP BY foodlist.id',
    );
  }

  create(createFoodlistDto: CreateFoodlistDto) {
    return this.foodlistRepository.save(createFoodlistDto);
  }

  findAll() {
    return this.foodlistRepository.find({
      relations: ['food'],
    });
  }

  findOne(id: number) {
    return this.foodlistRepository.findOne({ where: { id: id } });
  }

  async update(id: number, updateFoodlistDto: UpdateFoodlistDto) {
    try {
      const updatedFoodlist = await this.foodlistRepository.save({
        id,
        ...updateFoodlistDto,
      });
      return updatedFoodlist;
    } catch (e) {
      throw new NotFoundException();
    }
  }

  async remove(id: number) {
    const foodlist = await this.foodlistRepository.findOne({
      where: { id: id },
    });
    try {
      const deletedFoodlist = await this.foodlistRepository.softRemove(
        foodlist,
      );
      return deletedFoodlist;
    } catch (e) {
      throw new NotFoundException();
    }
  }
}
