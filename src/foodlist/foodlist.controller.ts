import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
} from '@nestjs/common';
import { FoodlistService } from './foodlist.service';
import { CreateFoodlistDto } from './dto/create-foodlist.dto';
import { UpdateFoodlistDto } from './dto/update-foodlist.dto';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';

@Controller('foodlist')
export class FoodlistController {
  constructor(private readonly foodlistService: FoodlistService) {}

  @UseGuards(JwtAuthGuard)
  @Post()
  create(@Body() createFoodlistDto: CreateFoodlistDto) {
    return this.foodlistService.create(createFoodlistDto);
  }

  @Get('/serve')
  findByServe() {
    return this.foodlistService.findByServeReady();
  }

  @Get()
  findAll() {
    return this.foodlistService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.foodlistService.findOne(+id);
  }

  @UseGuards(JwtAuthGuard)
  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateFoodlistDto: UpdateFoodlistDto,
  ) {
    return this.foodlistService.update(+id, updateFoodlistDto);
  }

  @UseGuards(JwtAuthGuard)
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.foodlistService.remove(+id);
  }
}
