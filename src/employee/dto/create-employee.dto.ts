import { IsNotEmpty } from 'class-validator';

export class CreateEmployeeDto {
  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  salary: number;

  @IsNotEmpty()
  workingHours: number;

  @IsNotEmpty()
  job: string;

  @IsNotEmpty()
  phoneNumber: string;
}
