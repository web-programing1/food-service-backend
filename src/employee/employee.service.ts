import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateEmployeeDto } from './dto/create-employee.dto';
import { UpdateEmployeeDto } from './dto/update-employee.dto';
import { Employee } from './entities/employee.entity';

@Injectable()
export class EmployeeService {
  constructor(
    @InjectRepository(Employee)
    private employeeRepository: Repository<Employee>,
  ) {}

  create(createEmployeeDto: CreateEmployeeDto) {
    return this.employeeRepository.save(createEmployeeDto);
  }

  findAll() {
    return this.employeeRepository.find();
  }

  findOne(id: number) {
    return this.employeeRepository.findOne({ where: { id: id } });
  }

  async update(id: number, updateEmployeeDto: UpdateEmployeeDto) {
    try {
      const updatedEmployee = await this.employeeRepository.save({
        id,
        ...updateEmployeeDto,
      });
      return updatedEmployee;
    } catch (e) {
      throw new NotFoundException();
    }
  }

  async remove(id: number) {
    const employee = await this.employeeRepository.findOne({
      where: { id: id },
    });
    try {
      const deletedEmployee = await this.employeeRepository.remove(employee);
      return deletedEmployee;
    } catch (e) {
      throw new NotFoundException();
    }
  }
}
