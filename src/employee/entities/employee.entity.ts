import { Checkinout } from 'src/checkinout/entities/checkinout.entity';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Employee {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    length: '32',
  })
  name: string;

  @Column()
  salary: number;

  @Column()
  workingHours: number;

  @Column()
  job: string;

  @Column()
  phoneNumber: string;

  @OneToMany(() => Checkinout, (checkinout) => checkinout.employee)
  checkinout: Checkinout[];
}
