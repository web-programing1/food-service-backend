import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectDataSource, InjectRepository } from '@nestjs/typeorm';
import { DataSource, Repository } from 'typeorm';
import { CreateCheckinoutDto } from './dto/create-checkinout.dto';
import { UpdateCheckinoutDto } from './dto/update-checkinout.dto';
import { Checkinout } from './entities/checkinout.entity';

@Injectable()
export class CheckinoutService {
  constructor(
    @InjectRepository(Checkinout)
    private checkinoutRepository: Repository<Checkinout>,
    @InjectDataSource()
    private dataSource: DataSource,
  ) {}

  create(createCheckinoutDto: CreateCheckinoutDto) {
    return this.checkinoutRepository.save(createCheckinoutDto);
  }

  getAllEmployee() {
    return this.dataSource.query(
      'SELECT checkinout.id,employeeid,name,checkInTime,checkOutTime FROM checkinout INNER JOIN employee ON checkinout.employeeId=employee.id ORDER BY checkInTime DESC',
    );
  }

  getEmployeeByEmployeeId(employeeId: number) {
    return this.dataSource.query(
      `SELECT checkinout.id,employeeid,name,checkInTime,checkOutTime 
      FROM checkinout 
      INNER JOIN employee 
      ON checkinout.employeeId=employee.id 
      WHERE employeeid=${employeeId} 
      ORDER BY checkInTime DESC`,
    );
  }

  findAll() {
    return this.checkinoutRepository.find({
      withDeleted: true,
      relations: ['employee'],
    });
  }

  findOne(id: number) {
    return this.checkinoutRepository.findOne({
      where: { id: id },
      withDeleted: true,
      relations: ['employee'],
    });
  }

  async update(id: number, updateCheckinoutDto: UpdateCheckinoutDto) {
    try {
      const updatedCheckinout = await this.checkinoutRepository.save({
        id,
        ...updateCheckinoutDto,
      });
      return updatedCheckinout;
    } catch (e) {
      throw new NotFoundException();
    }
  }

  async remove(id: number) {
    const checkOut = await this.checkinoutRepository.findOne({
      where: { employeeId: id },
    });
    try {
      const deletedCheckinout = await this.checkinoutRepository.softRemove(
        checkOut,
      );
      return deletedCheckinout;
    } catch (e) {
      throw new NotFoundException();
    }
  }
}
