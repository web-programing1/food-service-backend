import { Employee } from 'src/employee/entities/employee.entity';
import {
  PrimaryGeneratedColumn,
  CreateDateColumn,
  DeleteDateColumn,
  Column,
  Entity,
  ManyToOne,
} from 'typeorm';

@Entity()
export class Checkinout {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  employeeId: number;

  @CreateDateColumn()
  checkInTime: Date;

  @DeleteDateColumn()
  checkOutTime: Date;

  @ManyToOne(() => Employee, (employee) => employee.checkinout)
  employee: Employee;
}
