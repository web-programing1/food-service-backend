import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
  UseGuards,
} from '@nestjs/common';
import { CheckinoutService } from './checkinout.service';
import { CreateCheckinoutDto } from './dto/create-checkinout.dto';
import { UpdateCheckinoutDto } from './dto/update-checkinout.dto';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';

@UseGuards(JwtAuthGuard)
@Controller('checkinout')
export class CheckinoutController {
  constructor(private readonly checkinoutService: CheckinoutService) {}

  @Post()
  create(@Body() createCheckinoutDto: CreateCheckinoutDto) {
    return this.checkinoutService.create(createCheckinoutDto);
  }

  @Get()
  findAll(@Query() query: { employeeId?: number }) {
    if (query.employeeId) {
      return this.checkinoutService.getEmployeeByEmployeeId(query.employeeId);
    }
    return this.checkinoutService.getAllEmployee();
  }

  // @Get()
  // findAll() {
  //   return this.checkinoutService.findAll();
  // }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.checkinoutService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateCheckinoutDto: UpdateCheckinoutDto,
  ) {
    return this.checkinoutService.update(+id, updateCheckinoutDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.checkinoutService.remove(+id);
  }
}
