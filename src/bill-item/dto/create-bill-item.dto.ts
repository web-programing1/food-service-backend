import { IsNotEmpty, IsNumber, Min } from 'class-validator';
import { Bill } from 'src/bill/entities/bill.entity';

export class CreateBillItemDto {
  @IsNotEmpty()
  name: string;

  @IsNumber()
  amount: number;

  total: number;

  bill: Bill;
}
