import { Bill } from 'src/bill/entities/bill.entity';
import {
  PrimaryGeneratedColumn,
  Column,
  Entity,
  DeleteDateColumn,
  CreateDateColumn,
  ManyToOne,
} from 'typeorm';

@Entity()
export class BillItem {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  amount: number;

  @Column()
  total: number;

  @ManyToOne(() => Bill, (bill) => bill.billItems)
  bill: Bill;

  @CreateDateColumn()
  createDate: Date;

  @DeleteDateColumn()
  deletedDate: Date;
}
