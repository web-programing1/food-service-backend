import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateBillDto } from './dto/create-bill.dto';
import { UpdateBillDto } from './dto/update-bill.dto';
import { DataSource, Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Bill } from './entities/bill.entity';
import { BillItem } from 'src/bill-item/entities/bill-item.entity';

@Injectable()
export class BillService {
  constructor(
    private dataSource: DataSource,
    @InjectRepository(Bill)
    private billRepository: Repository<Bill>,
    @InjectRepository(BillItem)
    private billItemRepository: Repository<BillItem>,
  ) {}

  getSalesDayReport() {
    return this.dataSource.query(
      "SELECT strftime('%Y-%m-%d', b.createDate) AS date, COUNT(*) AS numBills, SUM(b.totalPrice) AS totalSales, (SELECT SUM(amount) FROM bill_item WHERE deletedDate IS NULL AND DATE(createDate) = DATE(b.createDate)) AS totalAmount FROM bill b WHERE b.deletedDate IS NULL GROUP BY strftime('%Y-%m-%d', b.createDate) ORDER BY date DESC",
    );
  }

  getSalesMonthReport() {
    return this.dataSource.query(
      "SELECT strftime('%Y-%m', b.createDate) AS date, COUNT(*) AS numBills, SUM(b.totalPrice) AS totalSales, (SELECT SUM(amount) FROM bill_item WHERE deletedDate IS NULL AND STRFTIME('%Y-%m', createDate) = STRFTIME('%Y-%m', b.createDate)) AS totalAmount FROM bill b WHERE b.deletedDate IS NULL GROUP BY strftime('%Y-%m', b.createDate) ORDER BY date DESC",
    );
  }

  getSalesYearReport() {
    return this.dataSource.query(
      "SELECT strftime('%Y', b.createDate) AS date, COUNT(*) AS numBills, SUM(b.totalPrice) AS totalSales, (SELECT SUM(amount) FROM bill_item WHERE deletedDate IS NULL AND STRFTIME('%Y', createDate) = STRFTIME('%Y', b.createDate)) AS totalAmount FROM bill b WHERE b.deletedDate IS NULL GROUP BY strftime('%Y', b.createDate) ORDER BY date DESC",
    );
  }

  getSalesDayFoodReport() {
    return this.dataSource.query(
      "SELECT name, SUM(amount) AS totalAmount FROM bill_item WHERE deletedDate IS NULL AND DATE(createDate) = DATE('now') GROUP BY name ORDER BY totalAmount DESC;",
    );
  }

  getSalesMonthFoodReport() {
    return this.dataSource.query(
      "SELECT bi.name, SUM(bi.amount) AS totalAmount FROM bill_item bi INNER JOIN bill b ON bi.billId = b.id WHERE b.deletedDate IS NULL AND STRFTIME('%Y-%m', b.createDate) = STRFTIME('%Y-%m', 'now') AND bi.deletedDate IS NULL GROUP BY bi.name ORDER BY totalAmount DESC",
    );
  }

  getSalesYearFoodReport() {
    return this.dataSource.query(
      "SELECT bi.name, SUM(bi.amount) AS totalAmount FROM bill_item bi INNER JOIN bill b ON bi.billId = b.id WHERE b.deletedDate IS NULL AND STRFTIME('%Y', b.createDate) = STRFTIME('%Y', 'now') AND bi.deletedDate IS NULL GROUP BY bi.name ORDER BY totalAmount DESC",
    );
  }

  async create(createBillDto: CreateBillDto) {
    console.log(createBillDto);
    createBillDto.totalPrice = 0;
    const bill = await this.billRepository.save(createBillDto);
    createBillDto.billItems.forEach(async (billItemData) => {
      const billItem = await this.billItemRepository.save(billItemData);
      billItem.bill = bill;
      bill.totalPrice += billItem.total;
      this.billItemRepository.save(billItem);
      this.billRepository.save(bill);
    });
    return this.billRepository.save(bill);
  }

  findAll() {
    return this.billRepository.find({
      relations: ['billItems'],
    });
  }

  findOne(id: number) {
    this.billRepository.findOne({
      where: { id: id },
      relations: ['billItems'],
    });
  }

  async update(id: number, updateBillDto: UpdateBillDto) {
    try {
      const updatedBill = await this.billRepository.save({
        id,
        ...updateBillDto,
      });
      return updatedBill;
    } catch (e) {
      throw new NotFoundException();
    }
  }

  async remove(id: number) {
    const bill = await this.billRepository.findOne({
      where: { id: id },
    });
    try {
      const deletedBill = await this.billRepository.softRemove(bill);
      return deletedBill;
    } catch (e) {
      throw new NotFoundException();
    }
  }
}
