import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
  Query,
} from '@nestjs/common';
import { BillService } from './bill.service';
import { CreateBillDto } from './dto/create-bill.dto';
import { UpdateBillDto } from './dto/update-bill.dto';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';

@Controller('bill')
export class BillController {
  constructor(private readonly billService: BillService) {}

  @UseGuards(JwtAuthGuard)
  @Post()
  create(@Body() createBillDto: CreateBillDto) {
    return this.billService.create(createBillDto);
  }

  @Get()
  findAll(
    @Query()
    query: {
      food?: number;
      day?: number;
      month?: number;
      year?: number;
    },
  ) {
    if (query.food) {
      if (query.day) {
        return this.billService.getSalesDayFoodReport();
      }
      if (query.month) {
        return this.billService.getSalesMonthFoodReport();
      }
      if (query.year) {
        return this.billService.getSalesYearFoodReport();
      }
    }
    if (query.day) {
      return this.billService.getSalesDayReport();
    }
    if (query.month) {
      return this.billService.getSalesMonthReport();
    }
    if (query.year) {
      return this.billService.getSalesYearReport();
    }
    return this.billService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.billService.findOne(+id);
  }

  @UseGuards(JwtAuthGuard)
  @Patch(':id')
  update(@Param('id') id: string, @Body() updateBillDto: UpdateBillDto) {
    return this.billService.update(+id, updateBillDto);
  }

  @UseGuards(JwtAuthGuard)
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.billService.remove(+id);
  }
}
