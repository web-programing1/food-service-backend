import { Module } from '@nestjs/common';
import { BillService } from './bill.service';
import { BillController } from './bill.controller';
import { BillItem } from 'src/bill-item/entities/bill-item.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Bill } from './entities/bill.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Bill, BillItem])],
  controllers: [BillController],
  providers: [BillService],
})
export class BillModule {}
