import { IsNotEmpty, IsNumber, Min } from 'class-validator';
import { BillItem } from 'src/bill-item/entities/bill-item.entity';

export class CreateBillDto {
  billItems: BillItem[];

  totalPrice: number;
}
