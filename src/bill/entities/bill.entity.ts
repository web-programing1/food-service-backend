import { BillItem } from 'src/bill-item/entities/bill-item.entity';
import {
  PrimaryGeneratedColumn,
  Column,
  Entity,
  DeleteDateColumn,
  CreateDateColumn,
  OneToMany,
} from 'typeorm';

@Entity()
export class Bill {
  @PrimaryGeneratedColumn()
  id: number;

  @OneToMany(() => BillItem, (billItem) => billItem.bill)
  billItems: BillItem[];

  @Column()
  totalPrice: number;

  @CreateDateColumn()
  createDate: Date;

  @DeleteDateColumn()
  deletedDate: Date;
}
