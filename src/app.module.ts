import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DataSource } from 'typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UsersModule } from './users/users.module';
import { User } from './users/entities/user.entity';
import { AuthModule } from './auth/auth.module';
import { FoodModule } from './food/food.module';
import { Food } from './food/entities/food.entity';
import { EmployeeModule } from './employee/employee.module';
import { Employee } from './employee/entities/employee.entity';
import { FoodlistModule } from './foodlist/foodlist.module';
import { Foodlist } from './foodlist/entities/foodlist.entity';
import { CheckinoutModule } from './checkinout/checkinout.module';
import { Checkinout } from './checkinout/entities/checkinout.entity';
import { TableModule } from './table/table.module';
import { Order } from './order/entities/order.entity';
import { OrderItem } from './order/entities/order-item';
import { Table } from './table/entities/table.entity';
import { OrderModule } from './order/order.module';
import { StockModule } from './stock/stock.module';
import { Stock } from './stock/entities/stock.entity';

@Module({
  imports: [
    TypeOrmModule.forRoot(
      // {
      //   type: 'sqlite',
      //   database: 'foodservice.sqlite',
      //   synchronize: true,
      //   migrations: [],
      //   entities: [
      //     User,
      //     Food,
      //     Employee,
      //     Foodlist,
      //     Checkinout,
      //     Order,
      //     OrderItem,
      //     Table,
      //     Stock,
      //   ],
      // },
      {
        type: 'mysql',
        host: 'localhost',
        port: 3306,
        username: 'dbproject',
        password: 'pass1234',
        database: 'dbproject',
        entities: [
          User,
          Food,
          Employee,
          Foodlist,
          Checkinout,
          Order,
          OrderItem,
          Table,
          Stock,
        ],
        synchronize: true,
      },
    ),

    UsersModule,
    AuthModule,
    FoodModule,
    FoodlistModule,
    EmployeeModule,
    FoodlistModule,
    CheckinoutModule,
    TableModule,
    OrderModule,
    StockModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor(private dataSource: DataSource) {}
}
