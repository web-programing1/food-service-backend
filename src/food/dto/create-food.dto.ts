import { IsNotEmpty, IsNumber, Min } from 'class-validator';

export class CreateFoodDto {
  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  ingredients: string;

  @IsNotEmpty()
  price: number;

  @IsNotEmpty()
  calories: number;

  @IsNotEmpty() // minutes
  preparationTime: number;

  @IsNotEmpty()
  category: string;

  @IsNotEmpty()
  img: string;
}
