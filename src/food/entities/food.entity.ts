import {
  PrimaryGeneratedColumn,
  Column,
  Entity,
  DeleteDateColumn,
} from 'typeorm';

@Entity()
export class Food {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  ingredients: string;

  @Column()
  price: number;

  @Column()
  calories: number;

  @Column() // minutes
  preparationTime: number;

  @Column()
  category: string;

  @DeleteDateColumn()
  deletedDate: Date;

  @Column()
  img: string;
}
