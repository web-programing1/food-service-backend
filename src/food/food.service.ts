import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateFoodDto } from './dto/create-food.dto';
import { UpdateFoodDto } from './dto/update-food.dto';
import { Food } from './entities/food.entity';

@Injectable()
export class FoodService {
  constructor(
    @InjectRepository(Food)
    private foodRepository: Repository<Food>,
  ) {}

  create(createFoodDto: CreateFoodDto) {
    return this.foodRepository.save(createFoodDto);
  }

  findAll() {
    return this.foodRepository.find();
  }

  findOne(id: number) {
    return this.foodRepository.findOne({ where: { id: id } });
  }

  async update(id: number, updateFoodDto: UpdateFoodDto) {
    try {
      const updatedFood = await this.foodRepository.save({
        id,
        ...updateFoodDto,
      });
      return updatedFood;
    } catch (e) {
      throw new NotFoundException();
    }
  }

  async remove(id: number) {
    const food = await this.foodRepository.findOne({
      where: { id: id },
    });
    try {
      const deletedFood = await this.foodRepository.softRemove(food);
      return deletedFood;
    } catch (e) {
      throw new NotFoundException();
    }
  }
}
