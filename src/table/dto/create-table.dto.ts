import { IsInt, IsNotEmpty, IsString, Length } from 'class-validator';

export class CreateTableDto {
  @IsNotEmpty()
  @IsInt()
  seats: number;

  @IsNotEmpty()
  @Length(1)
  @IsString()
  status: string;
}
