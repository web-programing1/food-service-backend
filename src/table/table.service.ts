import { Injectable, NotFoundException, UseGuards } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Table } from './entities/table.entity';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { Repository } from 'typeorm';
import { CreateTableDto } from './dto/create-table.dto';
import { UpdateTableDto } from './dto/update-table.dto';

@UseGuards(JwtAuthGuard)
@Injectable()
export class TableService {
  constructor(
    @InjectRepository(Table)
    private tablesRepository: Repository<Table>,
  ) {}
  create(createTableDto: CreateTableDto) {
    return this.tablesRepository.save(createTableDto);
  }

  findAll() {
    return this.tablesRepository.find();
  }

  async findOne(id: number) {
    const table = await this.tablesRepository.findOne({ where: { id: id } });
    if (!table) {
      throw new NotFoundException();
    }
    return table;
  }

  async update(id: number, updateTableDto: UpdateTableDto) {
    const table = await this.tablesRepository.findOneBy({ id: id });
    if (!table) {
      throw new NotFoundException();
    }
    const updateTable = { ...table, ...updateTableDto };
    return this.tablesRepository.save(updateTable);
  }

  remove(id: number) {
    return `Can't delete table ${id}`;
  }
}
