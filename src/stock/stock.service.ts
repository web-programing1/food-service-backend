import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateStockDto } from './dto/create-stock.dto';
import { UpdateStockDto } from './dto/update-stock.dto';
import { Stock } from './entities/stock.entity';

@Injectable()
export class StockService {
  constructor(
    @InjectRepository(Stock)
    private stockRepository: Repository<Stock>,
  ) {}

  create(createStockDto: CreateStockDto) {
    return this.stockRepository.save(createStockDto);
  }

  findAll() {
    return this.stockRepository.find();
  }

  findOne(id: number) {
    return this.stockRepository.findOne({ where: { id: id } });
  }

  async update(id: number, updateStockDto: UpdateStockDto) {
    try {
      const updatedStock = await this.stockRepository.save({
        id,
        ...updateStockDto,
      });
      return updatedStock;
    } catch (e) {
      throw new NotFoundException();
    }
  }

  async remove(id: number) {
    const stock = await this.stockRepository.findOne({
      where: { id: id },
    });
    try {
      const deletedStock = await this.stockRepository.remove(stock);
      return deletedStock;
    } catch (e) {
      throw new NotFoundException();
    }
  }
}
