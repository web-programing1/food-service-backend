import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
  UseGuards,
} from '@nestjs/common';
import { OrdersService } from './order.service';
import { CreateOrderDto } from './dto/create-order.dto';
import { UpdateOrderDto } from './dto/update-order.dto';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';

@Controller('order')
export class OrderController {
  constructor(private readonly orderService: OrdersService) {}

  @UseGuards(JwtAuthGuard)
  @Post()
  create(@Body() createOrderDto: CreateOrderDto) {
    return this.orderService.create(createOrderDto);
  }

  @Get()
  findAll(
    @Query()
    query: {
      tableId?: number;
      tableBillId?: number;
    },
  ) {
    if (query.tableBillId) {
      return this.orderService.getBillOrderByTableId(query.tableBillId);
    } else if (query.tableId) {
      return this.orderService.findByTableId(query.tableId);
    }
    return this.orderService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.orderService.findOne(+id);
  }

  @UseGuards(JwtAuthGuard)
  @Patch()
  updateBill(
    @Query()
    query: {
      tableId?: number;
    },
    @Param('id') id: string,
    @Body() updateOrderDto: UpdateOrderDto,
  ) {
    if (query.tableId) {
      return this.orderService.updateOrderBillByTableId(query.tableId);
    }
  }

  @UseGuards(JwtAuthGuard)
  @Patch(':id')
  update(
    @Query()
    query: {
      tableId?: number;
    },
    @Param('id') id: string,
    @Body() updateOrderDto: UpdateOrderDto,
  ) {
    if (query.tableId) {
      return this.orderService.updateOrderBillByTableId(query.tableId);
    } else {
      return this.orderService.update(+id, updateOrderDto);
    }
  }

  @UseGuards(JwtAuthGuard)
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.orderService.remove(+id);
  }
}
