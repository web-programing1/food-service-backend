import { Module } from '@nestjs/common';
import { OrdersService } from './order.service';
import { OrderController } from './order.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Order } from './entities/order.entity';
import { OrderItem } from './entities/order-item';
import { Food } from 'src/food/entities/food.entity';
import { Foodlist } from 'src/foodlist/entities/foodlist.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Order, OrderItem, Food, Foodlist])],
  controllers: [OrderController],
  providers: [OrdersService],
})
export class OrderModule {}
