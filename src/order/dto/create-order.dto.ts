import { IsNotEmpty, IsNumber } from 'class-validator';
import { CreateTableDto } from 'src/table/dto/create-table.dto';
import { Table } from 'src/table/entities/table.entity';

class CreatedOrderItemDto {
  @IsNotEmpty()
  @IsNumber()
  foodId: number;

  @IsNotEmpty()
  @IsNumber()
  amount: number;
}

export class CreateOrderDto {
  @IsNotEmpty()
  orderItems: CreatedOrderItemDto[];

  @IsNotEmpty()
  table: number;
}
