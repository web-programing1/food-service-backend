import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectDataSource, InjectRepository } from '@nestjs/typeorm';
import { Food } from 'src/food/entities/food.entity';
import { Foodlist } from 'src/foodlist/entities/foodlist.entity';
import { DataSource, Repository } from 'typeorm';
import { CreateOrderDto } from './dto/create-order.dto';
import { UpdateOrderDto } from './dto/update-order.dto';
import { OrderItem } from './entities/order-item';
import { Order } from './entities/order.entity';

@Injectable()
export class OrdersService {
  constructor(
    @InjectDataSource()
    private dataSource: DataSource,
    @InjectRepository(Order)
    private ordersRepository: Repository<Order>,
    @InjectRepository(Food)
    private foodsRepository: Repository<Food>,
    @InjectRepository(OrderItem)
    private orderItemsRepository: Repository<OrderItem>,
    @InjectRepository(Foodlist)
    private foodlistRepository: Repository<Foodlist>,
  ) {}

  findByTableId(tableId: number) {
    return this.dataSource.query(
      'SELECT * FROM "order" WHERE tableId == ? AND status == "pending"',
      [tableId],
    );
  }

  getBillOrderByTableId(tableId: number) {
    return this.dataSource.query(
      'SELECT DISTINCT food.name, COUNT(foodlist.id) as amount, food.price*COUNT(foodlist.id) as total FROM "order" INNER JOIN order_item ON orderId == "order".id INNER JOIN food ON food.id == order_item.foodId INNER JOIN foodlist ON foodlist.orderItemId == order_item.id WHERE tableId == ? AND status == "pending" AND foodlist.orderStatus == "เสร็จสิ้น" AND "order".deletedDate IS NULL GROUP BY food.name',
      [tableId],
    );
  }

  updateOrderBillByTableId(tableId: number) {
    return this.dataSource.query(
      'UPDATE "order" SET status = "paid" WHERE tableId = ?',
      [tableId],
    );
  }

  async create(createOrderDto: CreateOrderDto) {
    const order: Order = new Order();
    order.amount = 0;
    order.total = 0;
    order.status = 'pending';
    order.table = createOrderDto.table;
    await this.ordersRepository.save(order);
    const orderItems: OrderItem[] = await Promise.all(
      createOrderDto.orderItems.map(async (od) => {
        const orderItem = new OrderItem();
        orderItem.amount = od.amount;
        orderItem.food = await this.foodsRepository.findOneBy({
          id: od.foodId,
        });
        orderItem.name = orderItem.food.name;
        orderItem.price = orderItem.food.price;
        orderItem.total = orderItem.price * orderItem.amount;
        orderItem.order = order;
        return orderItem;
      }),
    );
    for (const orderItem of orderItems) {
      await this.orderItemsRepository.save(orderItem);
      for (let index = 0; index < orderItem.amount; index++) {
        const foodlist = this.foodlistRepository.create({
          food: orderItem.food,
          orderStatus: 'รอทำอาหาร',
          orderItem: orderItem,
        });
        await this.foodlistRepository.save(foodlist);
      }
      order.amount += orderItem.amount;
      order.total += orderItem.total;
    }
    this.ordersRepository.save(order);
    return await this.ordersRepository.findOne({
      where: { id: order.id },
      relations: ['orderItems'],
    });
  }

  findAll() {
    return this.ordersRepository.find({
      relations: ['orderItems', 'table'],
    });
  }

  findOne(id: number) {
    return this.ordersRepository.findOne({
      where: { id: id },
      relations: ['orderItems'],
    });
  }

  async update(id: number, updateOrderDto: UpdateOrderDto) {
    try {
      const updatedOrder = await this.ordersRepository.save({
        id,
        ...updateOrderDto,
      });
      return updatedOrder;
    } catch (e) {
      throw new NotFoundException();
    }
  }

  async remove(id: number) {
    const order = await this.ordersRepository.findOneBy({ id: id });
    return this.ordersRepository.softDelete(order);
  }
}
